package com.atlassian.jwt.exception;

/**
 * Indicates that the JWT's signature does not match its contents or the shared secret for the specified issuer.
 *
 * @since 1.0
 */
public class JwtSignatureMismatchException extends JwtVerificationException
{
    private String issuer;

    public JwtSignatureMismatchException(Exception cause)
    {
        super(cause);
    }

    /**
     * @deprecated Since 2.0.2, use the other constructors
     */
    @Deprecated
    public JwtSignatureMismatchException(String reason)
    {
        super(reason);
    }

    public JwtSignatureMismatchException(String reason, String issuer)
    {
        super(reason);
        this.issuer = issuer;
    }

    public String getIssuer() {
        return issuer;
    }
}
